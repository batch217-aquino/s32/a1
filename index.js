let http = require("http");

let port = 4000;

let server = http.createServer((req, res) => {
    // HTTP METHOD - GET
    // Get method means that we will be retrieving

    if (req.url == "/" && req.method == "GET"){
        res.writeHead(200, {"Content-Type" : "text/plain"});

        res.end("Welcome to Booking System.")
    }else if (req.url == "/profile" && req.method == "GET"){
        res.writeHead(200, {"Content-Type" : "text/plain"});

        res.end("Welcome to your profile!")
    }else if (req.url == "/courses" && req.method == "GET"){
        res.writeHead(200, {"Content-Type" : "text/plain"});

        res.end("Here’s our courses available")
    }
    // HTTP METHOD - POST
    // Post method means that we will be inserting data in the server

    if(req.url == "/addcourse" && req.method == "POST"){
        res.writeHead(200, {"Content-Type" : "text"})
        res.end("Add a course to our resources");
    }

    if(req.url == "/updatecourse" && req.method == "PUT"){
        res.writeHead(200, {"Content-Type" : "text"})
        res.end("Update a course to our resources ");
    }
    
    if(req.url == "/archivecourses" && req.method == "DELETE"){
        res.writeHead(200, {"Content-Type" : "text"})
        res.end("Archive courses to our resources");
    }




});

server.listen(port);

console.log(`Server is running at localhost: ${port}.`);